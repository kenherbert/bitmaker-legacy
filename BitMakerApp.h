//---------------------------------------------------------------------------
//
// Name:        BitMakerApp.h
// Author:      Firedancer Software
// Created:     21/05/2012 8:51:26 AM
// Description:
//
//---------------------------------------------------------------------------

#ifndef __BITMAKERFRMApp_h__
#define __BITMAKERFRMApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

#include <wx/cmdline.h>

class BitMakerFrmApp : public wxApp
{
	public:
		bool OnInit();
		wxCmdLineParser parser;
		wxString defaultProfilePath;
		wxString defaultConfigPath;
		int OnExit();
};

static const wxCmdLineEntryDesc g_cmdLineDesc [] =
{
	{ wxCMD_LINE_SWITCH, "h", "help", "Displays help on the command line parameters",
		wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
	{ wxCMD_LINE_OPTION, "p", "profilepath", "The default location to save and load profiles",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_OPTION, "c", "configpath", "The location of the config file",
		wxCMD_LINE_VAL_STRING, wxCMD_LINE_NEEDS_SEPARATOR },
	{ wxCMD_LINE_NONE }
};


#endif
#define IDI_APPICON 101
