//---------------------------------------------------------------------------
//
// Name:        BitMakerApp.cpp
// Author:      Firedancer Software
// Created:     21/05/2012 8:51:26 AM
// Description:
//
//---------------------------------------------------------------------------

#include "BitMakerApp.h"
#include "BitMakerFrm.h"
#include <wx/stdpaths.h>
#include <wx/filename.h>
#include <wx/cmdline.h>
#include <wx/protocol/http.h>

IMPLEMENT_APP(BitMakerFrmApp)

bool BitMakerFrmApp::OnInit()
{
	parser.SetDesc(g_cmdLineDesc);
	parser.SetCmdLine(argc, argv);
	parser.SetSwitchChars(wxT("-"));
	int iParseVal = parser.Parse();

	if(iParseVal != 0)
	{
		return 0;
	}

	wxHTTP::Initialize();

	wxString appPath = wxStandardPaths::Get().GetExecutablePath();

	defaultConfigPath = wxPathOnly(appPath);
 	parser.Found(wxT("c"), &defaultConfigPath);

	defaultProfilePath = wxPathOnly(appPath) + wxFileName::GetPathSeparator() + wxT("profiles");
	parser.Found(wxT("p"), &defaultProfilePath);

			if(!wxFileName::DirExists(defaultProfilePath))
			{
//				wxFileName::Mkdir(defaultProfilePath);
				if(!wxFileName::Mkdir(defaultProfilePath))
				{
					defaultProfilePath = appPath;
				}
			}


	BitMakerFrm* frame = new BitMakerFrm(defaultProfilePath, defaultConfigPath);
	SetTopWindow(frame);
	frame->SetIcon(wxICON(appicon));
	frame->Show();
	return true;
}

int BitMakerFrmApp::OnExit()
{
	return 0;
}
